// ==UserScript==
// @name         etrackCleanUp
// @namespace    http://tampermonkey.net/
// @version      68
// @description  Cleans up the etrack system for college
// @author       Rhys O'Connor
// @match        http://portfolio.3aaa.co.uk/*
// @grant        none
// @updateURL    https://gitlab.com/roconnor/ETrackCleanUp/raw/master/etrackCleanUp.user.js
// @downloadURL  https://gitlab.com/roconnor/ETrackCleanUp/raw/master/etrackCleanUp.user.js
// ==/UserScript==


function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000)); //makes the cookie
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires + ';path=/';
}

function getCookie(cookieName) { //gets the right cookie
    var name = cookieName;
    var cookieArray = document.cookie.split(';');

    for (var i = 0; i < cookieArray.length; i++) {
        if (cookieArray[i].includes(cookieName)) {
            var cookie = cookieArray[i];
            var substring = cookie.substring(name.length + 1);
            return substring.replace(/=/g, '');
        }
    }
    return '';
}

(function(window, document, undefined) {
    "use strict";

    (function e(t, n, r) {
        function s(o, u) {
            if (!n[o]) {
                if (!t[o]) {
                    var a = typeof require == "function" && require;
                    if (!u && a) return a(o, !0);
                    if (i) return i(o, !0);
                    var f = new Error("Cannot find module '" + o + "'");
                    throw f.code = "MODULE_NOT_FOUND", f
                }
                var l = n[o] = {
                    exports: {}
                };
                t[o][0].call(l.exports, function(e) {
                    var n = t[o][1][e];
                    return s(n ? n : e)
                }, l, l.exports, e, t, n, r)
            }
            return n[o].exports
        }
        var i = typeof require == "function" && require;
        for (var o = 0; o < r.length; o++) s(r[o]);
        return s
    })({
        1: [

            function(require, module, exports) {
                'use strict';

                var _interopRequireWildcard = function(obj) {
                    return obj && obj.__esModule ? obj : {
                        'default': obj
                    };
                };

                Object.defineProperty(exports, '__esModule', {
                    value: true
                });

                /*
                 * jQuery-like functions for manipulating the DOM
                 */

                var _hasClass$addClass$removeClass$escapeHtml$_show$show$_hide$hide$isDescendant$getTopMargin$fadeIn$fadeOut$fireClick$stopEventPropagation = require('./modules/handle-dom');

                /*
                 * Handy utilities
                 */

                var _extend$hexToRgb$isIE8$logStr$colorLuminance = require('./modules/utils');

                /*
                 *  Handle sweetAlert's DOM elements
                 */

                var _sweetAlertInitialize$getModal$getOverlay$getInput$setFocusStyle$openModal$resetInput$fixVerticalPosition = require('./modules/handle-swal-dom');

                // Handle button events and keyboard events

                var _handleButton$handleConfirm$handleCancel = require('./modules/handle-click');

                var _handleKeyDown = require('./modules/handle-key');

                var _handleKeyDown2 = _interopRequireWildcard(_handleKeyDown);

                // Default values

                var _defaultParams = require('./modules/default-params');

                var _defaultParams2 = _interopRequireWildcard(_defaultParams);

                var _setParameters = require('./modules/set-params');

                var _setParameters2 = _interopRequireWildcard(_setParameters);

                /*
                 * Remember state in cases where opening and handling a modal will fiddle with it.
                 * (We also use window.previousActiveElement as a global variable)
                 */
                var previousWindowKeyDown;
                var lastFocusedButton;

                /*
                 * Global sweetAlert function
                 * (this is what the user calls)
                 */
                var sweetAlert, swal;

                exports['default'] = sweetAlert = swal = function() {
                    var customizations = arguments[0];

                    _hasClass$addClass$removeClass$escapeHtml$_show$show$_hide$hide$isDescendant$getTopMargin$fadeIn$fadeOut$fireClick$stopEventPropagation.addClass(document.body, 'stop-scrolling');
                    _sweetAlertInitialize$getModal$getOverlay$getInput$setFocusStyle$openModal$resetInput$fixVerticalPosition.resetInput();

                    /*
                     * Use argument if defined or default value from params object otherwise.
                     * Supports the case where a default value is boolean true and should be
                     * overridden by a corresponding explicit argument which is boolean false.
                     */
                    function argumentOrDefault(key) {
                        var args = customizations;
                        return args[key] === undefined ? _defaultParams2['default'][key] : args[key];
                    }

                    if (customizations === undefined) {
                        _extend$hexToRgb$isIE8$logStr$colorLuminance.logStr('SweetAlert expects at least 1 attribute!');
                        return false;
                    }

                    var params = _extend$hexToRgb$isIE8$logStr$colorLuminance.extend({}, _defaultParams2['default']);

                    switch (typeof customizations) {

                        // Ex: swal("Hello", "Just testing", "info");
                        case 'string':
                            params.title = customizations;
                            params.text = arguments[1] || '';
                            params.type = arguments[2] || '';
                            break;

                            // Ex: swal({ title:"Hello", text: "Just testing", type: "info" });
                        case 'object':
                            if (customizations.title === undefined) {
                                _extend$hexToRgb$isIE8$logStr$colorLuminance.logStr('Missing "title" argument!');
                                return false;
                            }

                            params.title = customizations.title;

                            for (var customName in _defaultParams2['default']) {
                                params[customName] = argumentOrDefault(customName);
                            }

                            // Show "Confirm" instead of "OK" if cancel button is visible
                            params.confirmButtonText = params.showCancelButton ? 'Confirm' : _defaultParams2['default'].confirmButtonText;
                            params.confirmButtonText = argumentOrDefault('confirmButtonText');

                            // Callback function when clicking on "OK"/"Cancel"
                            params.doneFunction = arguments[1] || null;

                            break;

                        default:
                            _extend$hexToRgb$isIE8$logStr$colorLuminance.logStr('Unexpected type of argument! Expected "string" or "object", got ' + typeof customizations);
                            return false;

                    }

                    _setParameters2['default'](params);
                    _sweetAlertInitialize$getModal$getOverlay$getInput$setFocusStyle$openModal$resetInput$fixVerticalPosition.fixVerticalPosition();
                    _sweetAlertInitialize$getModal$getOverlay$getInput$setFocusStyle$openModal$resetInput$fixVerticalPosition.openModal(arguments[1]);

                    // Modal interactions
                    var modal = _sweetAlertInitialize$getModal$getOverlay$getInput$setFocusStyle$openModal$resetInput$fixVerticalPosition.getModal();

                    /*
                     * Make sure all modal buttons respond to all events
                     */
                    var $buttons = modal.querySelectorAll('button');
                    var buttonEvents = ['onclick', 'onmouseover', 'onmouseout', 'onmousedown', 'onmouseup', 'onfocus'];
                    var onButtonEvent = function onButtonEvent(e) {
                        return _handleButton$handleConfirm$handleCancel.handleButton(e, params, modal);
                    };

                    for (var btnIndex = 0; btnIndex < $buttons.length; btnIndex++) {
                        for (var evtIndex = 0; evtIndex < buttonEvents.length; evtIndex++) {
                            var btnEvt = buttonEvents[evtIndex];
                            $buttons[btnIndex][btnEvt] = onButtonEvent;
                        }
                    }

                    // Clicking outside the modal dismisses it (if allowed by user)
                    _sweetAlertInitialize$getModal$getOverlay$getInput$setFocusStyle$openModal$resetInput$fixVerticalPosition.getOverlay().onclick = onButtonEvent;

                    previousWindowKeyDown = window.onkeydown;

                    var onKeyEvent = function onKeyEvent(e) {
                        return _handleKeyDown2['default'](e, params, modal);
                    };
                    window.onkeydown = onKeyEvent;

                    window.onfocus = function() {
                        // When the user has focused away and focused back from the whole window.
                        setTimeout(function() {
                            // Put in a timeout to jump out of the event sequence.
                            // Calling focus() in the event sequence confuses things.
                            if (lastFocusedButton !== undefined) {
                                lastFocusedButton.focus();
                                lastFocusedButton = undefined;
                            }
                        }, 0);
                    };

                    // Show alert with enabled buttons always
                    swal.enableButtons();
                };

                /*
                 * Set default params for each popup
                 * @param {Object} userParams
                 */
                sweetAlert.setDefaults = swal.setDefaults = function(userParams) {
                    if (!userParams) {
                        throw new Error('userParams is required');
                    }
                    if (typeof userParams !== 'object') {
                        throw new Error('userParams has to be a object');
                    }

                    _extend$hexToRgb$isIE8$logStr$colorLuminance.extend(_defaultParams2['default'], userParams);
                };

                /*
                 * Animation when closing modal
                 */
                sweetAlert.close = swal.close = function() {
                    var modal = _sweetAlertInitialize$getModal$getOverlay$getInput$setFocusStyle$openModal$resetInput$fixVerticalPosition.getModal();

                    _hasClass$addClass$removeClass$escapeHtml$_show$show$_hide$hide$isDescendant$getTopMargin$fadeIn$fadeOut$fireClick$stopEventPropagation.fadeOut(_sweetAlertInitialize$getModal$getOverlay$getInput$setFocusStyle$openModal$resetInput$fixVerticalPosition.getOverlay(), 5);
                    _hasClass$addClass$removeClass$escapeHtml$_show$show$_hide$hide$isDescendant$getTopMargin$fadeIn$fadeOut$fireClick$stopEventPropagation.fadeOut(modal, 5);
                    _hasClass$addClass$removeClass$escapeHtml$_show$show$_hide$hide$isDescendant$getTopMargin$fadeIn$fadeOut$fireClick$stopEventPropagation.removeClass(modal, 'showSweetAlert');
                    _hasClass$addClass$removeClass$escapeHtml$_show$show$_hide$hide$isDescendant$getTopMargin$fadeIn$fadeOut$fireClick$stopEventPropagation.addClass(modal, 'hideSweetAlert');
                    _hasClass$addClass$removeClass$escapeHtml$_show$show$_hide$hide$isDescendant$getTopMargin$fadeIn$fadeOut$fireClick$stopEventPropagation.removeClass(modal, 'visible');

                    /*
                     * Reset icon animations
                     */
                    var $successIcon = modal.querySelector('.sa-icon.sa-success');
                    _hasClass$addClass$removeClass$escapeHtml$_show$show$_hide$hide$isDescendant$getTopMargin$fadeIn$fadeOut$fireClick$stopEventPropagation.removeClass($successIcon, 'animate');
                    _hasClass$addClass$removeClass$escapeHtml$_show$show$_hide$hide$isDescendant$getTopMargin$fadeIn$fadeOut$fireClick$stopEventPropagation.removeClass($successIcon.querySelector('.sa-tip'), 'animateSuccessTip');
                    _hasClass$addClass$removeClass$escapeHtml$_show$show$_hide$hide$isDescendant$getTopMargin$fadeIn$fadeOut$fireClick$stopEventPropagation.removeClass($successIcon.querySelector('.sa-long'), 'animateSuccessLong');

                    var $errorIcon = modal.querySelector('.sa-icon.sa-error');
                    _hasClass$addClass$removeClass$escapeHtml$_show$show$_hide$hide$isDescendant$getTopMargin$fadeIn$fadeOut$fireClick$stopEventPropagation.removeClass($errorIcon, 'animateErrorIcon');
                    _hasClass$addClass$removeClass$escapeHtml$_show$show$_hide$hide$isDescendant$getTopMargin$fadeIn$fadeOut$fireClick$stopEventPropagation.removeClass($errorIcon.querySelector('.sa-x-mark'), 'animateXMark');

                    var $warningIcon = modal.querySelector('.sa-icon.sa-warning');
                    _hasClass$addClass$removeClass$escapeHtml$_show$show$_hide$hide$isDescendant$getTopMargin$fadeIn$fadeOut$fireClick$stopEventPropagation.removeClass($warningIcon, 'pulseWarning');
                    _hasClass$addClass$removeClass$escapeHtml$_show$show$_hide$hide$isDescendant$getTopMargin$fadeIn$fadeOut$fireClick$stopEventPropagation.removeClass($warningIcon.querySelector('.sa-body'), 'pulseWarningIns');
                    _hasClass$addClass$removeClass$escapeHtml$_show$show$_hide$hide$isDescendant$getTopMargin$fadeIn$fadeOut$fireClick$stopEventPropagation.removeClass($warningIcon.querySelector('.sa-dot'), 'pulseWarningIns');

                    // Reset custom class (delay so that UI changes aren't visible)
                    setTimeout(function() {
                        var customClass = modal.getAttribute('data-custom-class');
                        _hasClass$addClass$removeClass$escapeHtml$_show$show$_hide$hide$isDescendant$getTopMargin$fadeIn$fadeOut$fireClick$stopEventPropagation.removeClass(modal, customClass);
                    }, 300);

                    // Make page scrollable again
                    _hasClass$addClass$removeClass$escapeHtml$_show$show$_hide$hide$isDescendant$getTopMargin$fadeIn$fadeOut$fireClick$stopEventPropagation.removeClass(document.body, 'stop-scrolling');

                    // Reset the page to its previous state
                    window.onkeydown = previousWindowKeyDown;
                    if (window.previousActiveElement) {
                        window.previousActiveElement.focus();
                    }
                    lastFocusedButton = undefined;
                    clearTimeout(modal.timeout);

                    return true;
                };

                /*
                 * Validation of the input field is done by user
                 * If something is wrong => call showInputError with errorMessage
                 */
                sweetAlert.showInputError = swal.showInputError = function(errorMessage) {
                    var modal = _sweetAlertInitialize$getModal$getOverlay$getInput$setFocusStyle$openModal$resetInput$fixVerticalPosition.getModal();

                    var $errorIcon = modal.querySelector('.sa-input-error');
                    _hasClass$addClass$removeClass$escapeHtml$_show$show$_hide$hide$isDescendant$getTopMargin$fadeIn$fadeOut$fireClick$stopEventPropagation.addClass($errorIcon, 'show');

                    var $errorContainer = modal.querySelector('.sa-error-container');
                    _hasClass$addClass$removeClass$escapeHtml$_show$show$_hide$hide$isDescendant$getTopMargin$fadeIn$fadeOut$fireClick$stopEventPropagation.addClass($errorContainer, 'show');

                    $errorContainer.querySelector('p').innerHTML = errorMessage;

                    setTimeout(function() {
                        sweetAlert.enableButtons();
                    }, 1);

                    modal.querySelector('input').focus();
                };

                /*
                 * Reset input error DOM elements
                 */
                sweetAlert.resetInputError = swal.resetInputError = function(event) {
                    // If press enter => ignore
                    if (event && event.keyCode === 13) {
                        return false;
                    }

                    var $modal = _sweetAlertInitialize$getModal$getOverlay$getInput$setFocusStyle$openModal$resetInput$fixVerticalPosition.getModal();

                    var $errorIcon = $modal.querySelector('.sa-input-error');
                    _hasClass$addClass$removeClass$escapeHtml$_show$show$_hide$hide$isDescendant$getTopMargin$fadeIn$fadeOut$fireClick$stopEventPropagation.removeClass($errorIcon, 'show');

                    var $errorContainer = $modal.querySelector('.sa-error-container');
                    _hasClass$addClass$removeClass$escapeHtml$_show$show$_hide$hide$isDescendant$getTopMargin$fadeIn$fadeOut$fireClick$stopEventPropagation.removeClass($errorContainer, 'show');
                };

                /*
                 * Disable confirm and cancel buttons
                 */
                sweetAlert.disableButtons = swal.disableButtons = function(event) {
                    var modal = _sweetAlertInitialize$getModal$getOverlay$getInput$setFocusStyle$openModal$resetInput$fixVerticalPosition.getModal();
                    var $confirmButton = modal.querySelector('button.confirm');
                    var $cancelButton = modal.querySelector('button.cancel');
                    $confirmButton.disabled = true;
                    $cancelButton.disabled = true;
                };

                /*
                 * Enable confirm and cancel buttons
                 */
                sweetAlert.enableButtons = swal.enableButtons = function(event) {
                    var modal = _sweetAlertInitialize$getModal$getOverlay$getInput$setFocusStyle$openModal$resetInput$fixVerticalPosition.getModal();
                    var $confirmButton = modal.querySelector('button.confirm');
                    var $cancelButton = modal.querySelector('button.cancel');
                    $confirmButton.disabled = false;
                    $cancelButton.disabled = false;
                };

                if (typeof window !== 'undefined') {
                    // The 'handle-click' module requires
                    // that 'sweetAlert' was set as global.
                    window.sweetAlert = window.swal = sweetAlert;
                } else {
                    _extend$hexToRgb$isIE8$logStr$colorLuminance.logStr('SweetAlert is a frontend module!');
                }
                module.exports = exports['default'];

            }, {
                "./modules/default-params": 2,
                "./modules/handle-click": 3,
                "./modules/handle-dom": 4,
                "./modules/handle-key": 5,
                "./modules/handle-swal-dom": 6,
                "./modules/set-params": 8,
                "./modules/utils": 9
            }
        ],
        2: [

            function(require, module, exports) {
                'use strict';

                Object.defineProperty(exports, '__esModule', {
                    value: true
                });
                var defaultParams = {
                    title: '',
                    text: '',
                    type: null,
                    allowOutsideClick: false,
                    showConfirmButton: true,
                    showCancelButton: false,
                    closeOnConfirm: true,
                    closeOnCancel: true,
                    confirmButtonText: 'OK',
                    confirmButtonColor: '#8CD4F5',
                    cancelButtonText: 'Cancel',
                    imageUrl: null,
                    imageSize: null,
                    timer: null,
                    customClass: '',
                    html: false,
                    animation: true,
                    allowEscapeKey: true,
                    inputType: 'text',
                    inputPlaceholder: '',
                    inputValue: '',
                    showLoaderOnConfirm: false
                };

                exports['default'] = defaultParams;
                module.exports = exports['default'];

            }, {}
        ],
        3: [

            function(require, module, exports) {
                'use strict';

                Object.defineProperty(exports, '__esModule', {
                    value: true
                });

                var _colorLuminance = require('./utils');

                var _getModal = require('./handle-swal-dom');

                var _hasClass$isDescendant = require('./handle-dom');

                /*
                 * User clicked on "Confirm"/"OK" or "Cancel"
                 */
                var handleButton = function handleButton(event, params, modal) {
                    var e = event || window.event;
                    var target = e.target || e.srcElement;

                    var targetedConfirm = target.className.indexOf('confirm') !== -1;
                    var targetedOverlay = target.className.indexOf('sweet-overlay') !== -1;
                    var modalIsVisible = _hasClass$isDescendant.hasClass(modal, 'visible');
                    var doneFunctionExists = params.doneFunction && modal.getAttribute('data-has-done-function') === 'true';

                    // Since the user can change the background-color of the confirm button programmatically,
                    // we must calculate what the color should be on hover/active
                    var normalColor, hoverColor, activeColor;
                    if (targetedConfirm && params.confirmButtonColor) {
                        normalColor = params.confirmButtonColor;
                        hoverColor = _colorLuminance.colorLuminance(normalColor, -0.04);
                        activeColor = _colorLuminance.colorLuminance(normalColor, -0.14);
                    }

                    function shouldSetConfirmButtonColor(color) {
                        if (targetedConfirm && params.confirmButtonColor) {
                            target.style.backgroundColor = color;
                        }
                    }

                    switch (e.type) {
                        case 'mouseover':
                            shouldSetConfirmButtonColor(hoverColor);
                            break;

                        case 'mouseout':
                            shouldSetConfirmButtonColor(normalColor);
                            break;

                        case 'mousedown':
                            shouldSetConfirmButtonColor(activeColor);
                            break;

                        case 'mouseup':
                            shouldSetConfirmButtonColor(hoverColor);
                            break;

                        case 'focus':
                            var $confirmButton = modal.querySelector('button.confirm');
                            var $cancelButton = modal.querySelector('button.cancel');

                            if (targetedConfirm) {
                                $cancelButton.style.boxShadow = 'none';
                            } else {
                                $confirmButton.style.boxShadow = 'none';
                            }
                            break;

                        case 'click':
                            var clickedOnModal = modal === target;
                            var clickedOnModalChild = _hasClass$isDescendant.isDescendant(modal, target);

                            // Ignore click outside if allowOutsideClick is false
                            if (!clickedOnModal && !clickedOnModalChild && modalIsVisible && !params.allowOutsideClick) {
                                break;
                            }

                            if (targetedConfirm && doneFunctionExists && modalIsVisible) {
                                handleConfirm(modal, params);
                            } else if (doneFunctionExists && modalIsVisible || targetedOverlay) {
                                handleCancel(modal, params);
                            } else if (_hasClass$isDescendant.isDescendant(modal, target) && target.tagName === 'BUTTON') {
                                sweetAlert.close();
                            }
                            break;
                    }
                };

                /*
                 *  User clicked on "Confirm"/"OK"
                 */
                var handleConfirm = function handleConfirm(modal, params) {
                    var callbackValue = true;

                    if (_hasClass$isDescendant.hasClass(modal, 'show-input')) {
                        callbackValue = modal.querySelector('input').value;

                        if (!callbackValue) {
                            callbackValue = '';
                        }
                    }

                    params.doneFunction(callbackValue);

                    if (params.closeOnConfirm) {
                        sweetAlert.close();
                    }
                    // Disable cancel and confirm button if the parameter is true
                    if (params.showLoaderOnConfirm) {
                        sweetAlert.disableButtons();
                    }
                };

                /*
                 *  User clicked on "Cancel"
                 */
                var handleCancel = function handleCancel(modal, params) {
                    // Check if callback function expects a parameter (to track cancel actions)
                    var functionAsStr = String(params.doneFunction).replace(/\s/g, '');
                    var functionHandlesCancel = functionAsStr.substring(0, 9) === 'function(' && functionAsStr.substring(9, 10) !== ')';

                    if (functionHandlesCancel) {
                        params.doneFunction(false);
                    }

                    if (params.closeOnCancel) {
                        sweetAlert.close();
                    }
                };

                exports['default'] = {
                    handleButton: handleButton,
                    handleConfirm: handleConfirm,
                    handleCancel: handleCancel
                };
                module.exports = exports['default'];

            }, {
                "./handle-dom": 4,
                "./handle-swal-dom": 6,
                "./utils": 9
            }
        ],
        4: [

            function(require, module, exports) {
                'use strict';

                Object.defineProperty(exports, '__esModule', {
                    value: true
                });
                var hasClass = function hasClass(elem, className) {
                    return new RegExp(' ' + className + ' ').test(' ' + elem.className + ' ');
                };

                var addClass = function addClass(elem, className) {
                    if (!hasClass(elem, className)) {
                        elem.className += ' ' + className;
                    }
                };

                var removeClass = function removeClass(elem, className) {
                    var newClass = ' ' + elem.className.replace(/[\t\r\n]/g, ' ') + ' ';
                    if (hasClass(elem, className)) {
                        while (newClass.indexOf(' ' + className + ' ') >= 0) {
                            newClass = newClass.replace(' ' + className + ' ', ' ');
                        }
                        elem.className = newClass.replace(/^\s+|\s+$/g, '');
                    }
                };

                var escapeHtml = function escapeHtml(str) {
                    var div = document.createElement('div');
                    div.appendChild(document.createTextNode(str));
                    return div.innerHTML;
                };

                var _show = function _show(elem) {
                    elem.style.opacity = '';
                    elem.style.display = 'block';
                };

                var show = function show(elems) {
                    if (elems && !elems.length) {
                        return _show(elems);
                    }
                    for (var i = 0; i < elems.length; ++i) {
                        _show(elems[i]);
                    }
                };

                var _hide = function _hide(elem) {
                    elem.style.opacity = '';
                    elem.style.display = 'none';
                };

                var hide = function hide(elems) {
                    if (elems && !elems.length) {
                        return _hide(elems);
                    }
                    for (var i = 0; i < elems.length; ++i) {
                        _hide(elems[i]);
                    }
                };

                var isDescendant = function isDescendant(parent, child) {
                    var node = child.parentNode;
                    while (node !== null) {
                        if (node === parent) {
                            return true;
                        }
                        node = node.parentNode;
                    }
                    return false;
                };

                var getTopMargin = function getTopMargin(elem) {
                    elem.style.left = '-9999px';
                    elem.style.display = 'block';

                    var height = elem.clientHeight,
                        padding;
                    if (typeof getComputedStyle !== 'undefined') {
                        // IE 8
                        padding = parseInt(getComputedStyle(elem).getPropertyValue('padding-top'), 10);
                    } else {
                        padding = parseInt(elem.currentStyle.padding);
                    }

                    elem.style.left = '';
                    elem.style.display = 'none';
                    return '-' + parseInt((height + padding) / 2) + 'px';
                };

                var fadeIn = function fadeIn(elem, interval) {
                    if (+elem.style.opacity < 1) {
                        interval = interval || 16;
                        elem.style.opacity = 0;
                        elem.style.display = 'block';
                        var last = +new Date();
                        var tick = (function(_tick) {
                            function tick() {
                                return _tick.apply(this, arguments);
                            }

                            tick.toString = function() {
                                return _tick.toString();
                            };

                            return tick;
                        })(function() {
                            elem.style.opacity = +elem.style.opacity + (new Date() - last) / 100;
                            last = +new Date();

                            if (+elem.style.opacity < 1) {
                                setTimeout(tick, interval);
                            }
                        });
                        tick();
                    }
                    elem.style.display = 'block'; //fallback IE8
                };

                var fadeOut = function fadeOut(elem, interval) {
                    interval = interval || 16;
                    elem.style.opacity = 1;
                    var last = +new Date();
                    var tick = (function(_tick2) {
                        function tick() {
                            return _tick2.apply(this, arguments);
                        }

                        tick.toString = function() {
                            return _tick2.toString();
                        };

                        return tick;
                    })(function() {
                        elem.style.opacity = +elem.style.opacity - (new Date() - last) / 100;
                        last = +new Date();

                        if (+elem.style.opacity > 0) {
                            setTimeout(tick, interval);
                        } else {
                            elem.style.display = 'none';
                        }
                    });
                    tick();
                };

                var fireClick = function fireClick(node) {
                    // Taken from http://www.nonobtrusive.com/2011/11/29/programatically-fire-crossbrowser-click-event-with-javascript/
                    // Then fixed for today's Chrome browser.
                    if (typeof MouseEvent === 'function') {
                        // Up-to-date approach
                        var mevt = new MouseEvent('click', {
                            view: window,
                            bubbles: false,
                            cancelable: true
                        });
                        node.dispatchEvent(mevt);
                    } else if (document.createEvent) {
                        // Fallback
                        var evt = document.createEvent('MouseEvents');
                        evt.initEvent('click', false, false);
                        node.dispatchEvent(evt);
                    } else if (document.createEventObject) {
                        node.fireEvent('onclick');
                    } else if (typeof node.onclick === 'function') {
                        node.onclick();
                    }
                };

                var stopEventPropagation = function stopEventPropagation(e) {
                    // In particular, make sure the space bar doesn't scroll the main window.
                    if (typeof e.stopPropagation === 'function') {
                        e.stopPropagation();
                        e.preventDefault();
                    } else if (window.event && window.event.hasOwnProperty('cancelBubble')) {
                        window.event.cancelBubble = true;
                    }
                };

                exports.hasClass = hasClass;
                exports.addClass = addClass;
                exports.removeClass = removeClass;
                exports.escapeHtml = escapeHtml;
                exports._show = _show;
                exports.show = show;
                exports._hide = _hide;
                exports.hide = hide;
                exports.isDescendant = isDescendant;
                exports.getTopMargin = getTopMargin;
                exports.fadeIn = fadeIn;
                exports.fadeOut = fadeOut;
                exports.fireClick = fireClick;
                exports.stopEventPropagation = stopEventPropagation;

            }, {}
        ],
        5: [

            function(require, module, exports) {
                'use strict';

                Object.defineProperty(exports, '__esModule', {
                    value: true
                });

                var _stopEventPropagation$fireClick = require('./handle-dom');

                var _setFocusStyle = require('./handle-swal-dom');

                var handleKeyDown = function handleKeyDown(event, params, modal) {
                    var e = event || window.event;
                    var keyCode = e.keyCode || e.which;

                    var $okButton = modal.querySelector('button.confirm');
                    var $cancelButton = modal.querySelector('button.cancel');
                    var $modalButtons = modal.querySelectorAll('button[tabindex]');

                    if ([9, 13, 32, 27].indexOf(keyCode) === -1) {
                        // Don't do work on keys we don't care about.
                        return;
                    }

                    var $targetElement = e.target || e.srcElement;

                    var btnIndex = -1; // Find the button - note, this is a nodelist, not an array.
                    for (var i = 0; i < $modalButtons.length; i++) {
                        if ($targetElement === $modalButtons[i]) {
                            btnIndex = i;
                            break;
                        }
                    }

                    if (keyCode === 9) {
                        // TAB
                        if (btnIndex === -1) {
                            // No button focused. Jump to the confirm button.
                            $targetElement = $okButton;
                        } else {
                            // Cycle to the next button
                            if (btnIndex === $modalButtons.length - 1) {
                                $targetElement = $modalButtons[0];
                            } else {
                                $targetElement = $modalButtons[btnIndex + 1];
                            }
                        }

                        _stopEventPropagation$fireClick.stopEventPropagation(e);
                        $targetElement.focus();

                        if (params.confirmButtonColor) {
                            _setFocusStyle.setFocusStyle($targetElement, params.confirmButtonColor);
                        }
                    } else {
                        if (keyCode === 13) {
                            if ($targetElement.tagName === 'INPUT') {
                                $targetElement = $okButton;
                                $okButton.focus();
                            }

                            if (btnIndex === -1) {
                                // ENTER/SPACE clicked outside of a button.
                                $targetElement = $okButton;
                            } else {
                                // Do nothing - let the browser handle it.
                                $targetElement = undefined;
                            }
                        } else if (keyCode === 27 && params.allowEscapeKey === true) {
                            $targetElement = $cancelButton;
                            _stopEventPropagation$fireClick.fireClick($targetElement, e);
                        } else {
                            // Fallback - let the browser handle it.
                            $targetElement = undefined;
                        }
                    }
                };

                exports['default'] = handleKeyDown;
                module.exports = exports['default'];

            }, {
                "./handle-dom": 4,
                "./handle-swal-dom": 6
            }
        ],
        6: [

            function(require, module, exports) {
                'use strict';

                var _interopRequireWildcard = function(obj) {
                    return obj && obj.__esModule ? obj : {
                        'default': obj
                    };
                };

                Object.defineProperty(exports, '__esModule', {
                    value: true
                });

                var _hexToRgb = require('./utils');

                var _removeClass$getTopMargin$fadeIn$show$addClass = require('./handle-dom');

                var _defaultParams = require('./default-params');

                var _defaultParams2 = _interopRequireWildcard(_defaultParams);

                /*
                 * Add modal + overlay to DOM
                 */

                var _injectedHTML = require('./injected-html');

                var _injectedHTML2 = _interopRequireWildcard(_injectedHTML);

                var modalClass = '.sweet-alert';
                var overlayClass = '.sweet-overlay';

                var sweetAlertInitialize = function sweetAlertInitialize() {
                    var sweetWrap = document.createElement('div');
                    sweetWrap.innerHTML = _injectedHTML2['default'];

                    // Append elements to body
                    while (sweetWrap.firstChild) {
                        document.body.appendChild(sweetWrap.firstChild);
                    }
                };

                /*
                 * Get DOM element of modal
                 */
                var getModal = (function(_getModal) {
                    function getModal() {
                        return _getModal.apply(this, arguments);
                    }

                    getModal.toString = function() {
                        return _getModal.toString();
                    };

                    return getModal;
                })(function() {
                    var $modal = document.querySelector(modalClass);

                    if (!$modal) {
                        sweetAlertInitialize();
                        $modal = getModal();
                    }

                    return $modal;
                });

                /*
                 * Get DOM element of input (in modal)
                 */
                var getInput = function getInput() {
                    var $modal = getModal();
                    if ($modal) {
                        return $modal.querySelector('input');
                    }
                };

                /*
                 * Get DOM element of overlay
                 */
                var getOverlay = function getOverlay() {
                    return document.querySelector(overlayClass);
                };

                /*
                 * Add box-shadow style to button (depending on its chosen bg-color)
                 */
                var setFocusStyle = function setFocusStyle($button, bgColor) {
                    var rgbColor = _hexToRgb.hexToRgb(bgColor);
                    $button.style.boxShadow = '0 0 2px rgba(' + rgbColor + ', 0.8), inset 0 0 0 1px rgba(0, 0, 0, 0.05)';
                };

                /*
                 * Animation when opening modal
                 */
                var openModal = function openModal(callback) {
                    var $modal = getModal();
                    _removeClass$getTopMargin$fadeIn$show$addClass.fadeIn(getOverlay(), 10);
                    _removeClass$getTopMargin$fadeIn$show$addClass.show($modal);
                    _removeClass$getTopMargin$fadeIn$show$addClass.addClass($modal, 'showSweetAlert');
                    _removeClass$getTopMargin$fadeIn$show$addClass.removeClass($modal, 'hideSweetAlert');

                    window.previousActiveElement = document.activeElement;
                    var $okButton = $modal.querySelector('button.confirm');
                    $okButton.focus();

                    setTimeout(function() {
                        _removeClass$getTopMargin$fadeIn$show$addClass.addClass($modal, 'visible');
                    }, 500);

                    var timer = $modal.getAttribute('data-timer');

                    if (timer !== 'null' && timer !== '') {
                        var timerCallback = callback;
                        $modal.timeout = setTimeout(function() {
                            var doneFunctionExists = (timerCallback || null) && $modal.getAttribute('data-has-done-function') === 'true';
                            if (doneFunctionExists) {
                                timerCallback(null);
                            } else {
                                sweetAlert.close();
                            }
                        }, timer);
                    }
                };

                /*
                 * Reset the styling of the input
                 * (for example if errors have been shown)
                 */
                var resetInput = function resetInput() {
                    var $modal = getModal();
                    var $input = getInput();

                    _removeClass$getTopMargin$fadeIn$show$addClass.removeClass($modal, 'show-input');
                    $input.value = _defaultParams2['default'].inputValue;
                    $input.setAttribute('type', _defaultParams2['default'].inputType);
                    $input.setAttribute('placeholder', _defaultParams2['default'].inputPlaceholder);

                    resetInputError();
                };

                var resetInputError = function resetInputError(event) {
                    // If press enter => ignore
                    if (event && event.keyCode === 13) {
                        return false;
                    }

                    var $modal = getModal();

                    var $errorIcon = $modal.querySelector('.sa-input-error');
                    _removeClass$getTopMargin$fadeIn$show$addClass.removeClass($errorIcon, 'show');

                    var $errorContainer = $modal.querySelector('.sa-error-container');
                    _removeClass$getTopMargin$fadeIn$show$addClass.removeClass($errorContainer, 'show');
                };

                /*
                 * Set "margin-top"-property on modal based on its computed height
                 */
                var fixVerticalPosition = function fixVerticalPosition() {
                    var $modal = getModal();
                    $modal.style.marginTop = _removeClass$getTopMargin$fadeIn$show$addClass.getTopMargin(getModal());
                };

                exports.sweetAlertInitialize = sweetAlertInitialize;
                exports.getModal = getModal;
                exports.getOverlay = getOverlay;
                exports.getInput = getInput;
                exports.setFocusStyle = setFocusStyle;
                exports.openModal = openModal;
                exports.resetInput = resetInput;
                exports.resetInputError = resetInputError;
                exports.fixVerticalPosition = fixVerticalPosition;

            }, {
                "./default-params": 2,
                "./handle-dom": 4,
                "./injected-html": 7,
                "./utils": 9
            }
        ],
        7: [

            function(require, module, exports) {
                "use strict";

                Object.defineProperty(exports, "__esModule", {
                    value: true
                });
                var injectedHTML =

                    // Dark overlay
                    "<div class=\"sweet-overlay\" tabIndex=\"-1\"></div>" +

                    // Modal
                    "<div class=\"sweet-alert\">" +

                    // Error icon
                    "<div class=\"sa-icon sa-error\">\n      <span class=\"sa-x-mark\">\n        <span class=\"sa-line sa-left\"></span>\n        <span class=\"sa-line sa-right\"></span>\n      </span>\n    </div>" +

                    // Warning icon
                    "<div class=\"sa-icon sa-warning\">\n      <span class=\"sa-body\"></span>\n      <span class=\"sa-dot\"></span>\n    </div>" +

                    // Info icon
                    "<div class=\"sa-icon sa-info\"></div>" +

                    // Success icon
                    "<div class=\"sa-icon sa-success\">\n      <span class=\"sa-line sa-tip\"></span>\n      <span class=\"sa-line sa-long\"></span>\n\n      <div class=\"sa-placeholder\"></div>\n      <div class=\"sa-fix\"></div>\n    </div>" + "<div class=\"sa-icon sa-custom\"></div>" +

                    // Title, text and input
                    "<h2>Title</h2>\n    <p>Text</p>\n    <fieldset>\n      <input type=\"text\" tabIndex=\"3\" />\n      <div class=\"sa-input-error\"></div>\n    </fieldset>" +

                    // Input errors
                    "<div class=\"sa-error-container\">\n      <div class=\"icon\">!</div>\n      <p>Not valid!</p>\n    </div>" +

                    // Cancel and confirm buttons
                    "<div class=\"sa-button-container\">\n      <button class=\"cancel\" tabIndex=\"2\">Cancel</button>\n      <div class=\"sa-confirm-button-container\">\n        <button class=\"confirm\" tabIndex=\"1\">OK</button>" +

                    // Loading animation
                    "<div class=\"la-ball-fall\">\n          <div></div>\n          <div></div>\n          <div></div>\n        </div>\n      </div>\n    </div>" +

                    // End of modal
                    "</div>";

                exports["default"] = injectedHTML;
                module.exports = exports["default"];

            }, {}
        ],
        8: [

            function(require, module, exports) {
                'use strict';

                Object.defineProperty(exports, '__esModule', {
                    value: true
                });

                var _isIE8 = require('./utils');

                var _getModal$getInput$setFocusStyle = require('./handle-swal-dom');

                var _hasClass$addClass$removeClass$escapeHtml$_show$show$_hide$hide = require('./handle-dom');

                var alertTypes = ['error', 'warning', 'info', 'success', 'input', 'prompt'];

                /*
                 * Set type, text and actions on modal
                 */
                var setParameters = function setParameters(params) {
                    var modal = _getModal$getInput$setFocusStyle.getModal();

                    var $title = modal.querySelector('h2');
                    var $text = modal.querySelector('p');
                    var $cancelBtn = modal.querySelector('button.cancel');
                    var $confirmBtn = modal.querySelector('button.confirm');

                    /*
                     * Title
                     */
                    $title.innerHTML = params.html ? params.title : _hasClass$addClass$removeClass$escapeHtml$_show$show$_hide$hide.escapeHtml(params.title).split('\n').join('<br>');

                    /*
                     * Text
                     */
                    $text.innerHTML = params.html ? params.text : _hasClass$addClass$removeClass$escapeHtml$_show$show$_hide$hide.escapeHtml(params.text || '').split('\n').join('<br>');
                    if (params.text) _hasClass$addClass$removeClass$escapeHtml$_show$show$_hide$hide.show($text);

                    /*
                     * Custom class
                     */
                    if (params.customClass) {
                        _hasClass$addClass$removeClass$escapeHtml$_show$show$_hide$hide.addClass(modal, params.customClass);
                        modal.setAttribute('data-custom-class', params.customClass);
                    } else {
                        // Find previously set classes and remove them
                        var customClass = modal.getAttribute('data-custom-class');
                        _hasClass$addClass$removeClass$escapeHtml$_show$show$_hide$hide.removeClass(modal, customClass);
                        modal.setAttribute('data-custom-class', '');
                    }

                    /*
                     * Icon
                     */
                    _hasClass$addClass$removeClass$escapeHtml$_show$show$_hide$hide.hide(modal.querySelectorAll('.sa-icon'));

                    if (params.type && !_isIE8.isIE8()) {
                        var _ret = (function() {

                            var validType = false;

                            for (var i = 0; i < alertTypes.length; i++) {
                                if (params.type === alertTypes[i]) {
                                    validType = true;
                                    break;
                                }
                            }

                            if (!validType) {
                                logStr('Unknown alert type: ' + params.type);
                                return {
                                    v: false
                                };
                            }

                            var typesWithIcons = ['success', 'error', 'warning', 'info'];
                            var $icon = undefined;

                            if (typesWithIcons.indexOf(params.type) !== -1) {
                                $icon = modal.querySelector('.sa-icon.' + 'sa-' + params.type);
                                _hasClass$addClass$removeClass$escapeHtml$_show$show$_hide$hide.show($icon);
                            }

                            var $input = _getModal$getInput$setFocusStyle.getInput();

                            // Animate icon
                            switch (params.type) {

                                case 'success':
                                    _hasClass$addClass$removeClass$escapeHtml$_show$show$_hide$hide.addClass($icon, 'animate');
                                    _hasClass$addClass$removeClass$escapeHtml$_show$show$_hide$hide.addClass($icon.querySelector('.sa-tip'), 'animateSuccessTip');
                                    _hasClass$addClass$removeClass$escapeHtml$_show$show$_hide$hide.addClass($icon.querySelector('.sa-long'), 'animateSuccessLong');
                                    break;

                                case 'error':
                                    _hasClass$addClass$removeClass$escapeHtml$_show$show$_hide$hide.addClass($icon, 'animateErrorIcon');
                                    _hasClass$addClass$removeClass$escapeHtml$_show$show$_hide$hide.addClass($icon.querySelector('.sa-x-mark'), 'animateXMark');
                                    break;

                                case 'warning':
                                    _hasClass$addClass$removeClass$escapeHtml$_show$show$_hide$hide.addClass($icon, 'pulseWarning');
                                    _hasClass$addClass$removeClass$escapeHtml$_show$show$_hide$hide.addClass($icon.querySelector('.sa-body'), 'pulseWarningIns');
                                    _hasClass$addClass$removeClass$escapeHtml$_show$show$_hide$hide.addClass($icon.querySelector('.sa-dot'), 'pulseWarningIns');
                                    break;

                                case 'input':
                                case 'prompt':
                                    $input.setAttribute('type', params.inputType);
                                    $input.value = params.inputValue;
                                    $input.setAttribute('placeholder', params.inputPlaceholder);
                                    _hasClass$addClass$removeClass$escapeHtml$_show$show$_hide$hide.addClass(modal, 'show-input');
                                    setTimeout(function() {
                                        $input.focus();
                                        $input.addEventListener('keyup', swal.resetInputError);
                                    }, 400);
                                    break;
                            }
                        })();

                        if (typeof _ret === 'object') {
                            return _ret.v;
                        }
                    }

                    /*
                     * Custom image
                     */
                    if (params.imageUrl) {
                        var $customIcon = modal.querySelector('.sa-icon.sa-custom');

                        $customIcon.style.backgroundImage = 'url(' + params.imageUrl + ')';
                        _hasClass$addClass$removeClass$escapeHtml$_show$show$_hide$hide.show($customIcon);

                        var _imgWidth = 80;
                        var _imgHeight = 80;

                        if (params.imageSize) {
                            var dimensions = params.imageSize.toString().split('x');
                            var imgWidth = dimensions[0];
                            var imgHeight = dimensions[1];

                            if (!imgWidth || !imgHeight) {
                                logStr('Parameter imageSize expects value with format WIDTHxHEIGHT, got ' + params.imageSize);
                            } else {
                                _imgWidth = imgWidth;
                                _imgHeight = imgHeight;
                            }
                        }

                        $customIcon.setAttribute('style', $customIcon.getAttribute('style') + 'width:' + _imgWidth + 'px; height:' + _imgHeight + 'px');
                    }

                    /*
                     * Show cancel button?
                     */
                    modal.setAttribute('data-has-cancel-button', params.showCancelButton);
                    if (params.showCancelButton) {
                        $cancelBtn.style.display = 'inline-block';
                    } else {
                        _hasClass$addClass$removeClass$escapeHtml$_show$show$_hide$hide.hide($cancelBtn);
                    }

                    /*
                     * Show confirm button?
                     */
                    modal.setAttribute('data-has-confirm-button', params.showConfirmButton);
                    if (params.showConfirmButton) {
                        $confirmBtn.style.display = 'inline-block';
                    } else {
                        _hasClass$addClass$removeClass$escapeHtml$_show$show$_hide$hide.hide($confirmBtn);
                    }

                    /*
                     * Custom text on cancel/confirm buttons
                     */
                    if (params.cancelButtonText) {
                        $cancelBtn.innerHTML = _hasClass$addClass$removeClass$escapeHtml$_show$show$_hide$hide.escapeHtml(params.cancelButtonText);
                    }
                    if (params.confirmButtonText) {
                        $confirmBtn.innerHTML = _hasClass$addClass$removeClass$escapeHtml$_show$show$_hide$hide.escapeHtml(params.confirmButtonText);
                    }

                    /*
                     * Custom color on confirm button
                     */
                    if (params.confirmButtonColor) {
                        // Set confirm button to selected background color
                        $confirmBtn.style.backgroundColor = params.confirmButtonColor;

                        // Set the confirm button color to the loading ring
                        $confirmBtn.style.borderLeftColor = params.confirmLoadingButtonColor;
                        $confirmBtn.style.borderRightColor = params.confirmLoadingButtonColor;

                        // Set box-shadow to default focused button
                        _getModal$getInput$setFocusStyle.setFocusStyle($confirmBtn, params.confirmButtonColor);
                    }

                    /*
                     * Allow outside click
                     */
                    modal.setAttribute('data-allow-outside-click', params.allowOutsideClick);

                    /*
                     * Callback function
                     */
                    var hasDoneFunction = params.doneFunction ? true : false;
                    modal.setAttribute('data-has-done-function', hasDoneFunction);

                    /*
                     * Animation
                     */
                    if (!params.animation) {
                        modal.setAttribute('data-animation', 'none');
                    } else if (typeof params.animation === 'string') {
                        modal.setAttribute('data-animation', params.animation); // Custom animation
                    } else {
                        modal.setAttribute('data-animation', 'pop');
                    }

                    /*
                     * Timer
                     */
                    modal.setAttribute('data-timer', params.timer);
                };

                exports['default'] = setParameters;
                module.exports = exports['default'];

            }, {
                "./handle-dom": 4,
                "./handle-swal-dom": 6,
                "./utils": 9
            }
        ],
        9: [

            function(require, module, exports) {
                'use strict';

                Object.defineProperty(exports, '__esModule', {
                    value: true
                });
                /*
                 * Allow user to pass their own params
                 */
                var extend = function extend(a, b) {
                    for (var key in b) {
                        if (b.hasOwnProperty(key)) {
                            a[key] = b[key];
                        }
                    }
                    return a;
                };

                /*
                 * Convert HEX codes to RGB values (#000000 -> rgb(0,0,0))
                 */
                var hexToRgb = function hexToRgb(hex) {
                    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
                    return result ? parseInt(result[1], 16) + ', ' + parseInt(result[2], 16) + ', ' + parseInt(result[3], 16) : null;
                };

                /*
                 * Check if the user is using Internet Explorer 8 (for fallbacks)
                 */
                var isIE8 = function isIE8() {
                    return window.attachEvent && !window.addEventListener;
                };

                /*
                 * IE compatible logging for developers
                 */
                var logStr = function logStr(string) {
                    if (window.console) {
                        // IE...
                        window.console.log('SweetAlert: ' + string);
                    }
                };

                /*
                 * Set hover, active and focus-states for buttons
                 * (source: http://www.sitepoint.com/javascript-generate-lighter-darker-color)
                 */
                var colorLuminance = function colorLuminance(hex, lum) {
                    // Validate hex string
                    hex = String(hex).replace(/[^0-9a-f]/gi, '');
                    if (hex.length < 6) {
                        hex = hex[0] + hex[0] + hex[1] + hex[1] + hex[2] + hex[2];
                    }
                    lum = lum || 0;

                    // Convert to decimal and change luminosity
                    var rgb = '#';
                    var c;
                    var i;

                    for (i = 0; i < 3; i++) {
                        c = parseInt(hex.substr(i * 2, 2), 16);
                        c = Math.round(Math.min(Math.max(0, c + c * lum), 255)).toString(16);
                        rgb += ('00' + c).substr(c.length);
                    }

                    return rgb;
                };

                exports.extend = extend;
                exports.hexToRgb = hexToRgb;
                exports.isIE8 = isIE8;
                exports.logStr = logStr;
                exports.colorLuminance = colorLuminance;

            }, {}
        ]
    }, {}, [1])


    /*
     * Use SweetAlert with RequireJS
     */

    if (typeof define === 'function' && define.amd) {
        define(function() {
            return sweetAlert;
        });
    } else if (typeof module !== 'undefined' && module.exports) {
        module.exports = sweetAlert;
    }

})(window, document);

$(function() {

    $('html').attr('style', 'overflow-x: initial !important'); //fixes dodgy sticky scroll

    $('body').attr('style', 'overflow-x: initial !important'); //fixes dodgy sticky scroll

    console.log('What\'re you lookin\' at? :P');
    //global operations
    $.get('http://portfolio.3aaa.co.uk/etrack/Messages/GetUnreadMessageCount', function(data) {

        if (getCookie('messagecount') === '') {
            setCookie('messagecount', data, 999999);
        } else {
            if (parseInt(getCookie('messagecount')) < parseInt(data)) {
                toastr.options.escapeHtml = true;
                toastr.options.preventDuplicates = true;
                toastr.options.progressBar = true;
                toastr.options.onclick = function() { window.location.replace('http://portfolio.3aaa.co.uk/etrack/Messages/Inbox?getNew=true'); }

                toastr["info"]('New message recieved! Click me to open it');
            }

            setCookie('messagecount', data, 99999);
        }

        $('a[href="/etrack/Action/MVCMenuClick/Messages"].iconButton.bigNavButton.hidden-xs span.badge.badge-important').remove();
        $('a[href="/etrack/Action/MVCMenuClick/Messages"].iconButton.bigNavButton.hidden-xs').append('<span class="badge badge-imporant">' + data + '</span>');
    });

    setInterval(function() {
        $.get('http://portfolio.3aaa.co.uk/etrack/Messages/GetUnreadMessageCount', function(data) {

            if (getCookie('messagecount') === '') {
                setCookie('messagecount', data, 999999);
            } else {
                if (parseInt(getCookie('messagecount')) < parseInt(data)) {
                    toastr.options.escapeHtml = true;
                    toastr.options.preventDuplicates = true;
                    toastr.options.progressBar = true;
                    toastr.options.onclick = function() { window.location.replace('http://portfolio.3aaa.co.uk/etrack/Messages/Inbox?getNew=true'); }

                    toastr["info"]('New message recieved! Click me to open it');
                }

                setCookie('messagecount', data, 99999);
            }

            $('a[href="/etrack/Action/MVCMenuClick/Messages"].iconButton.bigNavButton.hidden-xs span.badge.badge-important').remove();
            $('a[href="/etrack/Action/MVCMenuClick/Messages"].iconButton.bigNavButton.hidden-xs').append('<span class="badge badge-imporant">' + data + '</span>');
        });
    }, 10000);


    var prn = $('.productInfoStamp').parent();

    $('.productInfoStamp.visible-xs').remove(); //there's two headers...

    $('body').prepend('<style>.sweet-alert,.sweet-overlay{position:fixed;display:none}body.stop-scrolling{height:100%;overflow:hidden}.sweet-overlay{background-color:#000;-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=40)";background-color:rgba(0,0,0,.4);left:0;right:0;top:0;bottom:0;z-index:10000}.sweet-alert{background-color:#fff;font-family:"Open Sans","Helvetica Neue",Helvetica,Arial,sans-serif;width:478px;padding:17px;border-radius:5px;text-align:center;left:50%;top:50%;margin-left:-256px;margin-top:-200px;overflow:hidden;z-index:99999}@media all and (max-width:540px){.sweet-alert{width:auto;margin-left:0;margin-right:0;left:15px;right:15px}}.sweet-alert h2{color:#575757;font-size:30px;text-align:center;font-weight:600;text-transform:none;position:relative;margin:25px 0;padding:0;line-height:40px;display:block}.sweet-alert p{color:#797979;font-size:16px;font-weight:300;position:relative;text-align:inherit;float:none;margin:0;padding:0;line-height:normal}.sweet-alert fieldset{border:none;position:relative}.sweet-alert .sa-error-container{background-color:#f1f1f1;margin-left:-17px;margin-right:-17px;overflow:hidden;padding:0 10px;max-height:0;webkit-transition:padding .15s,max-height .15s;transition:padding .15s,max-height .15s}.sweet-alert .sa-error-container.show{padding:10px 0;max-height:100px;webkit-transition:padding .2s,max-height .2s;transition:padding .25s,max-height .25s}.sweet-alert .sa-error-container .icon{display:inline-block;width:24px;height:24px;border-radius:50%;background-color:#ea7d7d;color:#fff;line-height:24px;text-align:center;margin-right:3px}.sweet-alert .sa-error-container p{display:inline-block}.sweet-alert .sa-input-error{position:absolute;top:29px;right:26px;width:20px;height:20px;opacity:0;-webkit-transform:scale(.5);transform:scale(.5);-webkit-transform-origin:50% 50%;transform-origin:50% 50%;-webkit-transition:all .1s;transition:all .1s}.sweet-alert .sa-input-error::after,.sweet-alert .sa-input-error::before{content:"";width:20px;height:6px;background-color:#f06e57;border-radius:3px;position:absolute;top:50%;margin-top:-4px;left:50%;margin-left:-9px}.sweet-alert .sa-input-error::before{-webkit-transform:rotate(-45deg);transform:rotate(-45deg)}.sweet-alert .sa-input-error::after{-webkit-transform:rotate(45deg);transform:rotate(45deg)}.sweet-alert .sa-input-error.show{opacity:1;-webkit-transform:scale(1);transform:scale(1)}.sweet-alert input{width:100%;box-sizing:border-box;border-radius:3px;border:1px solid #d7d7d7;height:43px;margin-top:10px;margin-bottom:17px;font-size:18px;box-shadow:inset 0 1px 1px rgba(0,0,0,.06);padding:0 12px;display:none;-webkit-transition:all .3s;transition:all .3s}.sweet-alert input:focus{outline:0;box-shadow:0 0 3px #c4e6f5;border:1px solid #b4dbed}.sweet-alert input:focus::-moz-placeholder{transition:opacity .3s 30ms ease;opacity:.5}.sweet-alert input:focus:-ms-input-placeholder{transition:opacity .3s 30ms ease;opacity:.5}.sweet-alert input:focus::-webkit-input-placeholder{transition:opacity .3s 30ms ease;opacity:.5}.sweet-alert input::-moz-placeholder{color:#bdbdbd}.sweet-alert input:-ms-input-placeholder{color:#bdbdbd}.sweet-alert input::-webkit-input-placeholder{color:#bdbdbd}.sweet-alert.show-input input{display:block}.sweet-alert .sa-confirm-button-container{display:inline-block;position:relative}.sweet-alert .la-ball-fall{position:absolute;left:50%;top:50%;margin-left:-27px;margin-top:4px;opacity:0;visibility:hidden}.sweet-alert button{background-color:#8CD4F5;color:#fff;border:none;box-shadow:none;font-size:17px;font-weight:500;-webkit-border-radius:4px;border-radius:5px;padding:10px 32px;margin:26px 5px 0;cursor:pointer}.sweet-alert button:focus{outline:0;box-shadow:0 0 2px rgba(128,179,235,.5),inset 0 0 0 1px rgba(0,0,0,.05)}.sweet-alert button:hover{background-color:#7ecff4}.sweet-alert button:active{background-color:#5dc2f1}.sweet-alert button.cancel{background-color:#C1C1C1}.sweet-alert button.cancel:hover{background-color:#b9b9b9}.sweet-alert button.cancel:active{background-color:#a8a8a8}.sweet-alert button.cancel:focus{box-shadow:rgba(197,205,211,.8) 0 0 2px,rgba(0,0,0,.0470588) 0 0 0 1px inset!important}.sweet-alert button[disabled]{opacity:.6;cursor:default}.sweet-alert button.confirm[disabled]{color:transparent}.sweet-alert button.confirm[disabled]~.la-ball-fall{opacity:1;visibility:visible;transition-delay:0s}.sweet-alert button::-moz-focus-inner{border:0}.sweet-alert[data-has-cancel-button=false] button{box-shadow:none!important}.sweet-alert[data-has-confirm-button=false][data-has-cancel-button=false]{padding-bottom:40px}.sweet-alert .sa-icon{width:80px;height:80px;border:4px solid gray;-webkit-border-radius:40px;border-radius:50%;margin:20px auto;padding:0;position:relative;box-sizing:content-box}.sweet-alert .sa-icon.sa-error{border-color:#F27474}.sweet-alert .sa-icon.sa-error .sa-x-mark{position:relative;display:block}.sweet-alert .sa-icon.sa-error .sa-line{position:absolute;height:5px;width:47px;background-color:#F27474;display:block;top:37px;border-radius:2px}.sweet-alert .sa-icon.sa-error .sa-line.sa-left{-webkit-transform:rotate(45deg);transform:rotate(45deg);left:17px}.sweet-alert .sa-icon.sa-error .sa-line.sa-right{-webkit-transform:rotate(-45deg);transform:rotate(-45deg);right:16px}.sweet-alert .sa-icon.sa-warning{border-color:#F8BB86}.sweet-alert .sa-icon.sa-warning .sa-body{position:absolute;width:5px;height:47px;left:50%;top:10px;-webkit-border-radius:2px;border-radius:2px;margin-left:-2px;background-color:#F8BB86}.sweet-alert .sa-icon.sa-warning .sa-dot{position:absolute;width:7px;height:7px;-webkit-border-radius:50%;border-radius:50%;margin-left:-3px;left:50%;bottom:10px;background-color:#F8BB86}.sweet-alert .sa-icon.sa-info::after,.sweet-alert .sa-icon.sa-info::before{content:"";background-color:#C9DAE1;position:absolute}.sweet-alert .sa-icon.sa-info{border-color:#C9DAE1}.sweet-alert .sa-icon.sa-info::before{width:5px;height:29px;left:50%;bottom:17px;border-radius:2px;margin-left:-2px}.sweet-alert .sa-icon.sa-info::after{width:7px;height:7px;border-radius:50%;margin-left:-3px;top:19px}.sweet-alert .sa-icon.sa-success{border-color:#A5DC86}.sweet-alert .sa-icon.sa-success::after,.sweet-alert .sa-icon.sa-success::before{content:"";position:absolute;width:60px;height:120px;background:#fff}.sweet-alert .sa-icon.sa-success::before{-webkit-border-radius:120px 0 0 120px;border-radius:120px 0 0 120px;top:-7px;left:-33px;-webkit-transform:rotate(-45deg);transform:rotate(-45deg);-webkit-transform-origin:60px 60px;transform-origin:60px 60px}.sweet-alert .sa-icon.sa-success::after{-webkit-border-radius:0 120px 120px 0;border-radius:0 120px 120px 0;top:-11px;left:30px;-webkit-transform:rotate(-45deg);transform:rotate(-45deg);-webkit-transform-origin:0 60px;transform-origin:0 60px}.sweet-alert .sa-icon.sa-success .sa-placeholder{width:80px;height:80px;border:4px solid rgba(165,220,134,.2);-webkit-border-radius:40px;border-radius:50%;box-sizing:content-box;position:absolute;left:-4px;top:-4px;z-index:2}.sweet-alert .sa-icon.sa-success .sa-fix{width:5px;height:90px;background-color:#fff;position:absolute;left:28px;top:8px;z-index:1;-webkit-transform:rotate(-45deg);transform:rotate(-45deg)}.sweet-alert .sa-icon.sa-success .sa-line{height:5px;background-color:#A5DC86;display:block;border-radius:2px;position:absolute;z-index:2}.sweet-alert .sa-icon.sa-success .sa-line.sa-tip{width:25px;left:14px;top:46px;-webkit-transform:rotate(45deg);transform:rotate(45deg)}.sweet-alert .sa-icon.sa-success .sa-line.sa-long{width:47px;right:8px;top:38px;-webkit-transform:rotate(-45deg);transform:rotate(-45deg)}.sweet-alert .sa-icon.sa-custom{background-size:contain;border-radius:0;border:none;background-position:center center;background-repeat:no-repeat}@-webkit-keyframes showSweetAlert{0%{transform:scale(.7);-webkit-transform:scale(.7)}45%{transform:scale(1.05);-webkit-transform:scale(1.05)}80%{transform:scale(.95);-webkit-transform:scale(.95)}100%{transform:scale(1);-webkit-transform:scale(1)}}@keyframes showSweetAlert{0%{transform:scale(.7);-webkit-transform:scale(.7)}45%{transform:scale(1.05);-webkit-transform:scale(1.05)}80%{transform:scale(.95);-webkit-transform:scale(.95)}100%{transform:scale(1);-webkit-transform:scale(1)}}@-webkit-keyframes hideSweetAlert{0%{transform:scale(1);-webkit-transform:scale(1)}100%{transform:scale(.5);-webkit-transform:scale(.5)}}@keyframes hideSweetAlert{0%{transform:scale(1);-webkit-transform:scale(1)}100%{transform:scale(.5);-webkit-transform:scale(.5)}}@-webkit-keyframes slideFromTop{0%{top:0}100%{top:50%}}@keyframes slideFromTop{0%{top:0}100%{top:50%}}@-webkit-keyframes slideToTop{0%{top:50%}100%{top:0}}@keyframes slideToTop{0%{top:50%}100%{top:0}}@-webkit-keyframes slideFromBottom{0%{top:70%}100%{top:50%}}@keyframes slideFromBottom{0%{top:70%}100%{top:50%}}@-webkit-keyframes slideToBottom{0%{top:50%}100%{top:70%}}@keyframes slideToBottom{0%{top:50%}100%{top:70%}}.showSweetAlert[data-animation=pop]{-webkit-animation:showSweetAlert .3s;animation:showSweetAlert .3s}.showSweetAlert[data-animation=none]{-webkit-animation:none;animation:none}.showSweetAlert[data-animation=slide-from-top]{-webkit-animation:slideFromTop .3s;animation:slideFromTop .3s}.showSweetAlert[data-animation=slide-from-bottom]{-webkit-animation:slideFromBottom .3s;animation:slideFromBottom .3s}.hideSweetAlert[data-animation=pop]{-webkit-animation:hideSweetAlert .2s;animation:hideSweetAlert .2s}.hideSweetAlert[data-animation=none]{-webkit-animation:none;animation:none}.hideSweetAlert[data-animation=slide-from-top]{-webkit-animation:slideToTop .4s;animation:slideToTop .4s}.hideSweetAlert[data-animation=slide-from-bottom]{-webkit-animation:slideToBottom .3s;animation:slideToBottom .3s}@-webkit-keyframes animateSuccessTip{0%,54%{width:0;left:1px;top:19px}70%{width:50px;left:-8px;top:37px}84%{width:17px;left:21px;top:48px}100%{width:25px;left:14px;top:45px}}@keyframes animateSuccessTip{0%,54%{width:0;left:1px;top:19px}70%{width:50px;left:-8px;top:37px}84%{width:17px;left:21px;top:48px}100%{width:25px;left:14px;top:45px}}@-webkit-keyframes animateSuccessLong{0%,65%{width:0;right:46px;top:54px}84%{width:55px;right:0;top:35px}100%{width:47px;right:8px;top:38px}}@keyframes animateSuccessLong{0%,65%{width:0;right:46px;top:54px}84%{width:55px;right:0;top:35px}100%{width:47px;right:8px;top:38px}}@-webkit-keyframes rotatePlaceholder{0%,5%{transform:rotate(-45deg);-webkit-transform:rotate(-45deg)}100%,12%{transform:rotate(-405deg);-webkit-transform:rotate(-405deg)}}@keyframes rotatePlaceholder{0%,5%{transform:rotate(-45deg);-webkit-transform:rotate(-45deg)}100%,12%{transform:rotate(-405deg);-webkit-transform:rotate(-405deg)}}.animateSuccessTip{-webkit-animation:animateSuccessTip .75s;animation:animateSuccessTip .75s}.animateSuccessLong{-webkit-animation:animateSuccessLong .75s;animation:animateSuccessLong .75s}.sa-icon.sa-success.animate::after{-webkit-animation:rotatePlaceholder 4.25s ease-in;animation:rotatePlaceholder 4.25s ease-in}@-webkit-keyframes animateErrorIcon{0%{transform:rotateX(100deg);-webkit-transform:rotateX(100deg);opacity:0}100%{transform:rotateX(0);-webkit-transform:rotateX(0);opacity:1}}@keyframes animateErrorIcon{0%{transform:rotateX(100deg);-webkit-transform:rotateX(100deg);opacity:0}100%{transform:rotateX(0);-webkit-transform:rotateX(0);opacity:1}}.animateErrorIcon{-webkit-animation:animateErrorIcon .5s;animation:animateErrorIcon .5s}@-webkit-keyframes animateXMark{0%,50%{transform:scale(.4);-webkit-transform:scale(.4);margin-top:26px;opacity:0}80%{transform:scale(1.15);-webkit-transform:scale(1.15);margin-top:-6px}100%{transform:scale(1);-webkit-transform:scale(1);margin-top:0;opacity:1}}@keyframes animateXMark{0%,50%{transform:scale(.4);-webkit-transform:scale(.4);margin-top:26px;opacity:0}80%{transform:scale(1.15);-webkit-transform:scale(1.15);margin-top:-6px}100%{transform:scale(1);-webkit-transform:scale(1);margin-top:0;opacity:1}}.animateXMark{-webkit-animation:animateXMark .5s;animation:animateXMark .5s}@-webkit-keyframes pulseWarning{0%{border-color:#F8D486}100%{border-color:#F8BB86}}@keyframes pulseWarning{0%{border-color:#F8D486}100%{border-color:#F8BB86}}.pulseWarning{-webkit-animation:pulseWarning .75s infinite alternate;animation:pulseWarning .75s infinite alternate}@-webkit-keyframes pulseWarningIns{0%{background-color:#F8D486}100%{background-color:#F8BB86}}@keyframes pulseWarningIns{0%{background-color:#F8D486}100%{background-color:#F8BB86}}.pulseWarningIns{-webkit-animation:pulseWarningIns .75s infinite alternate;animation:pulseWarningIns .75s infinite alternate}@-webkit-keyframes rotate-loading{0%{transform:rotate(0)}100%{transform:rotate(360deg)}}@keyframes rotate-loading{0%{transform:rotate(0)}100%{transform:rotate(360deg)}}.sweet-alert .sa-icon.sa-error .sa-line.sa-left{-ms-transform:rotate(45deg)\9}.sweet-alert .sa-icon.sa-error .sa-line.sa-right{-ms-transform:rotate(-45deg)\9}.sweet-alert .sa-icon.sa-success{border-color:transparent\9}.sweet-alert .sa-icon.sa-success .sa-line.sa-tip{-ms-transform:rotate(45deg)\9}.sweet-alert .sa-icon.sa-success .sa-line.sa-long{-ms-transform:rotate(-45deg)\9}.la-ball-fall,.la-ball-fall>div{position:relative;-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box}.la-ball-fall{display:block;font-size:0;color:#fff;width:54px;height:18px}.la-ball-fall.la-dark{color:#333}.la-ball-fall>div{display:inline-block;float:none;background-color:currentColor;border:0 solid currentColor;width:10px;height:10px;margin:4px;border-radius:100%;opacity:0;-webkit-animation:ball-fall 1s ease-in-out infinite;-moz-animation:ball-fall 1s ease-in-out infinite;-o-animation:ball-fall 1s ease-in-out infinite;animation:ball-fall 1s ease-in-out infinite}.la-ball-fall>div:nth-child(1){-webkit-animation-delay:-.2s;-moz-animation-delay:-.2s;-o-animation-delay:-.2s;animation-delay:-.2s}.la-ball-fall>div:nth-child(2){-webkit-animation-delay:-.1s;-moz-animation-delay:-.1s;-o-animation-delay:-.1s;animation-delay:-.1s}.la-ball-fall>div:nth-child(3){-webkit-animation-delay:0s;-moz-animation-delay:0s;-o-animation-delay:0s;animation-delay:0s}.la-ball-fall.la-sm{width:26px;height:8px}.la-ball-fall.la-sm>div{width:4px;height:4px;margin:2px}.la-ball-fall.la-2x{width:108px;height:36px}.la-ball-fall.la-2x>div{width:20px;height:20px;margin:8px}.la-ball-fall.la-3x{width:162px;height:54px}.la-ball-fall.la-3x>div{width:30px;height:30px;margin:12px}@-webkit-keyframes ball-fall{0%{opacity:0;-webkit-transform:translateY(-145%);transform:translateY(-145%)}10%,90%{opacity:.5}20%,80%{opacity:1;-webkit-transform:translateY(0);transform:translateY(0)}100%{opacity:0;-webkit-transform:translateY(145%);transform:translateY(145%)}}@-moz-keyframes ball-fall{0%{opacity:0;-moz-transform:translateY(-145%);transform:translateY(-145%)}10%,90%{opacity:.5}20%,80%{opacity:1;-moz-transform:translateY(0);transform:translateY(0)}100%{opacity:0;-moz-transform:translateY(145%);transform:translateY(145%)}}@-o-keyframes ball-fall{0%{opacity:0;-o-transform:translateY(-145%);transform:translateY(-145%)}10%,90%{opacity:.5}20%,80%{opacity:1;-o-transform:translateY(0);transform:translateY(0)}100%{opacity:0;-o-transform:translateY(145%);transform:translateY(145%)}}@keyframes ball-fall{0%{opacity:0;-webkit-transform:translateY(-145%);-moz-transform:translateY(-145%);-o-transform:translateY(-145%);transform:translateY(-145%)}10%,90%{opacity:.5}20%,80%{opacity:1;-webkit-transform:translateY(0);-moz-transform:translateY(0);-o-transform:translateY(0);transform:translateY(0)}100%{opacity:0;-webkit-transform:translateY(145%);-moz-transform:translateY(145%);-o-transform:translateY(145%);transform:translateY(145%)}}</style>');

    $('body').prepend('<style>.wishCompleted {background: rgb(255, 204, 69);}#fixedbutton {position: fixed;bottom: 0px;right: 0px;}.podbar{bottom:0;position:fixed;z-index:150;_position:absolute;_top:expression(eval(document.documentElement.scrollTop+(document.documentElement.clientHeight-this.offsetHeight)));height:35px;}</style>');

    $('.modal-content').removeClass('modal-sm').addClass('modal-lg'); // sizes up the page modals

    $('.eTrack-full-menu ul li').each(function() {
        $('#ExtraLinks').append($(this)); //adds the header buttons to the main menu
    });

    $('.dropdown.pull-right').remove(); //removes the weird dropdown

    $('.iconButton').each(function() {
        $(this).hover(function() {
            $(this).css('background-color', '#FFFFFF !important'); //removes the nashty hover on the header buttons
        });
    });

    $('.iconButton').filter(function() {
        return ($(this).children('span').first().text() == 'Training materials' && $(this).children('span').first().attr('style') == 'white-space: nowrap') || $(this).text() == 'Training materials' //renames Training materials
    }).empty().prepend('<i class="icon-copy"></i>').append('<span>Training</span>');

    $('#divNavigation').attr('class', 'divNav col-md-9');


    $('.iconButton').filter(function() {
        return $(this).children('span').first().text() == 'Mappings' || $(this).text() == 'Mappings' //removes all mappings options
    }).closest('li').remove();


    $('.iconButton').filter(function() {
        return $(this).children('span').first().text() == 'Home' || $(this).text() == 'Home' //removes dupe Home
    }).closest('li').slice(1).remove();

    $('.iconButton').filter(function() {
        return $(this).children('span').first().text() == 'Messages' || $(this).text() == 'Messages' //removes dupe Messages
    }).closest('li').slice(1).remove();



    $('.logotop').remove(); //deletes the logo
    $('.breadcrumb').remove(); //removes breadcrumb
    $('#divMiddle').prepend('<hr style="border-top: 3px solid #eee !important; padding-left: 0px !important" />'); // adds a rule under the header buttons in place of the breadcrumb


    //gets username
    var username = $('#AccountControl_Label1').text(); //gets username

    //removes the log out and chnge password links (tables??)
    $('#divBanner').find('table').each(function() {
        $(this).remove();
    });

    //recreates the user details
    $('#UserDetails').empty();
    $('#UserDetails').css('padding-top', '5px');
    $('#UserDetails').append('Welcome, ' + username + '!<br />'); //makes the banner nicer
    $('#UserDetails').append('<a href="/etrack/Account/Logout">Log Out</a> | <a href="/etrack/Account/ChangePassword">Change Password</a> | <a href="/">Home</a> | <a href="#" data-toggle="modal" data-target="#helpModal">Help</a>');

    //adds the Help modal
    $('body').append('<div id="helpModal" class="modal fade" role="dialog"></div>');
    $('#helpModal').append('<div class="modal-dialog"></div>');
    $('#helpModal div.modal-dialog').append('<div class="modal-content" style="overflow-y: auto;"></div>');
    $('#helpModal div.modal-dialog div.modal-content').append('<div class="modal-header"><button type="button" class="close" data-dismiss="modal">&times;</button><h4 class="modal-title">Help guide</h4></div>');
    $('#helpModal div.modal-dialog div.modal-content').append('<div class="modal-body" style="overflow-y: auto !important"><h4>Welcome to ETrack V2! Please see below for a getting started guide</h4></div>');
    $('#helpModal div.modal-dialog div.modal-content div.modal-body').append('<p>Welcome to ETrack V2! This was written by <a href="mailto:rhys.oconnor@outlook.com">Rhys O\'Connor</a> in an attempt to improve the ETrack system for myself and everyone else using it.<br /><br /> This modal is just a quick getting started guide to finding your way around, and a quick explanation of what has changed</p>');
    $('#helpModal div.modal-dialog div.modal-content div.modal-body').append('<h5>What\'s changed?</h5><p>A better question would be, what hasn\'t changed? As you can see, I\'ve had to keep the default layout as this isn\'t a new application. This may shock you, but ETrack V2 is actually just a script that gets injected into the old system, and then edits it all around! Despite this, there is a large plethora of changes that can be found below:</p>');
    $('#helpModal div.modal-dialog div.modal-content div.modal-body').append('<h5>How to use it?</h5><p>Underneath the glitter, this is still the same system. Many features are still accessible and haven\'t changed. However, there are many new features which I will run through now.</p>');
    $('#helpModal div.modal-dialog div.modal-content div.modal-body').append('<ul><li>-Added an initialization popup (for assessor details)</li><br /><li>-Fixed the feedback area</li><br /><li>-Added a new feedback popup</li><br /><li>-Allowed the ticking of units (a tick can mean whatever you want it to, but my idea is in the legend)</li><br /><li>-removed dropdown at top of page</li><br /><li>-fixed top bar</li><br /><li>-fixed visits</li><br /><li>-and much much more!</li></ul>');
    $('#helpModal div.modal-dialog div.modal-content div.modal-body').append('<h5>Unit ticking</h5><p>One of the biggest features was the ability to tick units. This is a large feature and is still a work in progress. However, it is currently working and can be used easily. To tick a unit off, all you have to do is click one of the checkboxes next to the unit number, then reload the page to bring the tick image into effect. You can also untick units this way. Due to the original usage of this eature (ticking off done but unmarked units), this isn\'t available for completed units. Please email me if you wish this to change, or just change it yourself (the code for this is on line 1427, next to the .hasClass if statement)</p>');
    $('#helpModal div.modal-dialog div.modal-content div.modal-body').append('<h5>Automatic login</h5><p>A smaller yet just as helpful feature is the auto login feature. When you login, your details will be saved and automatically used so long as you tick the \'remember me\' box. This will then locally store your username and password, and use them every time you visit ETrack</p>');
    $('#helpModal div.modal-dialog div.modal-content div.modal-body').append('<h5>New feedback</h5><p>One of the more recent features is the new feedback popup. When you login and there is new feedback available since the last time, you will get a notification telling you that there is new feedback available, who it\'s from, and when it was uploaded. From here you can open and view it, or just dismiss it.</p>');
    $('#helpModal div.modal-dialog div.modal-content div.modal-body').append('<h5>General bugfixes</h5><p>I have also implemented many UI and code bug fixes to the ETrack system, such as overflowing text (pff, fixed it), broken links (did that while sleeping), awkward modals (awkward no more), and plenty more!</p>');
    $('#helpModal div.modal-dialog div.modal-content').append('<div class="modal-footer"></div>');
    $('#helpModal div.modal-dialog div.modal-content div.modal-footer').append('<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>');

    $('#ExtraLinks').append('<li id="aboutLink"><a href="#" class="iconButton bigNavButton hidden-xs" style="background-color: rgb(255, 255, 255);"><i style="height:' + $('i.icon-rss').height() + '" class="icon-question"></i><span style="white-space: nowrap">About</span></a></li>')

    //stops the timeout

    $('#divSessionTimeOutTimeLeft').remove();

    if (window.location.href.toLowerCase().includes('http://portfolio.3aaa.co.uk/etrack/loginpage.aspx')) {
        loginPage();
    } else {
        //area for authenticated code ONLY!! (The user is logged in)
        document.querySelector('#aboutLink').onclick = function() {
            swal({
                    title: "ETrack V2",
                    text: "Created by Rhys O'Connor 2016",
                    type: "success",
                    showCancelButton: true,
                    confirmButtonColor: "#ADD8E6",
                    confirmButtonText: "View on Gitlab",
                    cancelButtonColor: "#ADD8E6",
                    cancelButtonText: "Cool!",
                    closeOnCancel: true
                },
                function() {
                    window.location.replace('https://gitlab.com/roconnor/ETrackCleanUp');
                });
        };
    }

    if (window.location.href.toLowerCase().includes('portfolio.3aaa.co.uk/etrack/trainee/index')) {
        homePage();
    }

    if (window.location.href.toLowerCase().includes('portfolio.3aaa.co.uk/etrack/messages')) { //the messages page actually links to submodules for each part (sent, inbox etc.), but this will effect all of those.
        messagingPage();
    }

    if (window.location.href.toLowerCase().includes('portfolio.3aaa.co.uk/etrack/traineeextras/trevidence')) {
        filePage();
    }

    //add calls to new page functions here
})

function filePage() {
    $('#divMiddle').append('<table id="fileList" class="table table-hover"><thead><tr><td>Ref No.</td><td>Type</td><td>Description</td><td>Uploaded</td><td>Download</td></tr></thead><tbody id="tbodyLoop"></tbody></table>');

    $('tr.fileLink').each(function() {
        var link = $(this).attr('href');
        var filename = $(this).attr('filename');
        var refnumber = $(this).children('td:nth-child(1)').first().text();
        var type = $(this).children('td:nth-child(3)').first().text();
        var description = $(this).children('td:nth-child(4)').first().text();
        var uploadDate = $(this).children('td:nth-child(5)').first().text();

        var splitter = '</td><td>';

        $('table#fileList tbody#tbodyLoop').append('<tr style="cursor: default !important"><td>' + refnumber + splitter + type + splitter + description + splitter + uploadDate + splitter + '<a href="' + link + '"><i style="text-decoration: none !important" class="icon icon-download"></i></a></td></tr>');

        $(this).remove();
    });

    $('table.table.eTrack-table.hidden-xs').remove();

    $('ul.pagination').parent().detach().appendTo('#divMiddle');
}

function loginPage() {

    $('#rememberLoginCheckBox').prop('checked', false); //the checkbox is checked by default, so we need to remove that

    var cookie = getCookie('loginDetails'); //gets the login details

    var username; //global to scope variables
    var password;

    if (cookie !== '') //if the cookie exists
    {
        username = cookie.split(':')[0].replace(/=/g, ''); //gets the username
        password = cookie.split(':')[1]; //gets the password

        $('#txtUserName').val(username); //sets the username
        $('#txtPassword').val(password); //sets the password

        __doPostBack('LinkButton1', ''); //'clicks' the login button
    }

    $('#LinkButton1').click(function() { //checks for login click
        if ($('#rememberLoginCheckBox').prop('checked', true)) //if the user wants to be remembered
        {
            username = $('#txtUserName').val(); //set the username from the textbox
            password = $('#txtPassword').val(); //set the password from the textbox

            setCookie('loginDetails', username + ':' + password, 99999); //sets the cookie (so hungry)

            __doPostBack('LinkButton1', ''); //'clicks' the button
        }
    });
}

function homePage() {
    $('.infolegend').append('<div class="legendItem"><div class="qualStatus overdue" style="background: #FFCC45 !important"></div>Done (Not marked)</div>'); //adds legend item for done not marked

    $('.visitFeedback').css('overflow-y', 'auto').css('height', '200px'); //makes the feedback box scrollable and a fixed height, stopping text overflow

    $('.unitBreakDown').css('padding-top', '30px'); //makes the unit breakdown a bit lower and stops overflow on Mobile view

    if (getCookie('assessorDetails') === '') //makes the welcome modal
    {
        //creates the modal
        $('body').append('<div data-backdrop="static" data-keyboard="false" id="assessorModal" class="modal fade" role="dialog"></div>');
        $('#assessorModal').append('<div class="modal-dialog"></div>');
        $('#assessorModal div.modal-dialog').append('<div class="modal-content" style="overflow-y: auto;"></div>');
        $('#assessorModal div.modal-dialog div.modal-content').append('<div class="modal-header"><button type="button" class="close" data-dismiss="modal">&times;</button><h4 class="modal-title">Assessor</h4></div>');
        $('#assessorModal div.modal-dialog div.modal-content').append('<div class="modal-body"><p>Welcome to ETrack V2! Please start by entering the values of your Assessor: </p></div>');
        $('#assessorModal div.modal-dialog div.modal-content div.modal-body').append('<input class="form-control" type="text" id="assessorFullname" placeholder="Full Name..." style="width: 50%; margin: 10px;"/>');
        $('#assessorModal div.modal-dialog div.modal-content div.modal-body').append('<input class="form-control" type="text" id="assessorEmail" placeholder="Email..." style="width: 50%; margin: 10px;"/>');
        $('#assessorModal div.modal-dialog div.modal-content div.modal-body').append('<input class="form-control" type="text" id="assessorPhone" placeholder="Phone Number..." style="width: 50%; margin: 10px;"/>');
        $('#assessorModal div.modal-dialog div.modal-content div.modal-body').append('<button style="margin: 10px;" class="btn btn-info" id="assessorSubmitBtn">Save!</button>');
        $('#assessorModal div.modal-dialog div.modal-content').append('<div class="modal-footer"></div>');
        $('#assessorModal div.modal-dialog div.modal-content div.modal-footer').append('<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>');

        //fades everything apart from the modal
        $('body *').not('#assessorModal').not('#assessorModal div').not('#assessorModal div div').not('#assessorModal div div div').not('#assessorModal div div div *').each(function() { //mmmmmmm... chained selectors...
            $(this).css('box-shadow', '0px 0px 1px 1px rgba(255,255,255,0) !important').css('text-shadow', '0px 0px 10px rgba(51, 51, 51, 0) !important').css('transform', 'scale(0.9) !important').css('opacity', '0.8'); //gives everything a box shadow and gives opacity
        });

        $('#assessorModal').modal('show'); //shows the modal

        $('#assessorSubmitBtn').click(function() { //creates the cookie with the Assessor details inside
            var fn = $('#assessorFullname').val();
            var em = $('#assessorEmail').val();
            var pn = $('#assessorPhone').val();

            setCookie('assessorDetails', fn + ',' + em + ',' + pn, 9999999);
            location.reload(); //reload the page to bring the assessor cookie into action
        });
    } else {
        var assessorCookie = getCookie('assessorDetails').split(',');

        $('.assessorDetails').empty(); // removes the rubbish assessor details
        $('.assessorDetails').append('<p class="info">Your Assessor is: ' + assessorCookie[0].replace('=', '') + '</p>'); //adds assessor name
        $('.assessorDetails').append('<p><i class="icon-envelope"></i> <a href="mailto:' + assessorCookie[1] + '">' + assessorCookie[1] + '</a></p>'); // Adds email
        $('.assessorDetails').append('<p><i class="icon-phone"></i> <a href="tel:' + assessorCookie[2] + '">' + assessorCookie[2] + '</a></p>'); //adds phone
    }

    $('.elementResultGroup').remove(); // gets rid of the squares in the unitBlocks

    $('.unitBlock').each(function() {
        $(this).children('span').remove();
        $(this).css('height', '72px !important');
        var element = $(this);

        //$(this).attr('data-modcode', $(this).attr('data-modcode').substring(9)); DO NOT ENABLE! Creates ambiguity between blocks (02 & 302 etc.)

        var modcode = $(this).attr('data-modcode'); //the unit code
        var link = $(this).attr('href'); //the unit page link

        $(this).children().each(function() {
            if ($(this).hasClass('unitElementResult')) //removes the annoying squares on the unitBlocks
            {
                $(this).remove();
            }
        });
        var pc = false;

        if ($(this).hasClass('completed')) {
            $(this).append('<p style="font-size: 15px">Completed</p>'); // foreach unit block, if it's completed then write completed, otherwise in progress
        } else if ($(this).hasClass('inProgress')) {
            $(this).append('<p style="font-size: 15px">In Progress</p>'); //adds the in progress text

            var cookieArr;
            if (getCookie('completedUnits') === '') //if cookie doesn't exist
            {
                setCookie('completedUnits', "", 9999999); //create a blank cookie

                cookieArr = getCookie('completedUnits').split(','); //has to have a value otherwise the foreach will break. (will be empty array)
            } else {
                cookieArr = getCookie('completedUnits').split(',');
            }

            cookieArr.forEach(function(cookieThis) {
                if (cookieThis === modcode) {
                    pc = true;

                    console.log('checked module code: ' + modcode);
                    $(element).removeClass('inProgress').addClass('wishCompleted');
                    $(element).children('p').text('Done');
                }
            });

            if (pc === true) {
                $(this).children('p').before('<input type="checkbox" id="personalCompletion" checked style="width: 15px !important; height: 15px !important;"></input>');
            } else if (pc === false) {
                $(this).children('p').before('<input type"checkbox" id="personalCompletion" style="width: 13px !important; height: 13px !important;"></input>');
            }
        }

        $(this).children('p').wrap('<a href="' + $(this).attr('href') + '"></a>');
        $(this).removeAttr('href'); //FIX: linked to undefined as the href was removed before injection, swapped statements

        $(this).find('#personalCompletion').click(function() {
            if (pc === false) {
                $(this).closest('a').removeClass('inProgress').addClass('wishCompleted');
                $(this).closest('a').children('p').text('Done');
                $(this).closest('p').append('<input type="checkbox" id="personalCompletion" checked></input>');

                var addCode = modcode;
                var cookie = getCookie('completedUnits');
                setCookie('completedUnits', cookie + ',' + addCode, 9999999);
                pc = true;
            } else {
                $(this).closest('a').removeClass('wishCompleted').addClass('inProgress');
                $(this).closest('a').children('p').text('In Progress');
                $(this).closest('p').append('<input type="checkbox" id="personalCompletion"></input>');

                var otherCookie = getCookie('completedUnits').split(',');
                otherCookie.splice(otherCookie.indexOf(modcode), 1);

                setCookie('completedUnits', otherCookie, 9999999);
                pc = false;
            }
            setCookie('completedUnits', cookie2, 9999999);
        });
    });


    $('#moduleHome_Home_thVisits p.further').remove(); // gets rid of the 00:00 in next visit

    var lastReview = $('#feedbackPopup').find('tbody').children('tr:nth-child(2)').first().children('td:nth-child(3)').text(); //gets the last review date and time

    $('.visitFeedback p.info').text('Latest feedback from ' + $('#feedbackPopup').find('tbody').children('tr:nth-child(2)').first().children('td:nth-child(1)').text());

    if (getCookie('lastReview') === '') //if the cookie doesn't exist
    {
        setCookie('lastReview', lastReview, 999999); //create it with the most recent review
    } else if (getCookie('lastReview').replace(/=/g, '') !== lastReview) {
        toastr.options.escapeHtml = true;
        toastr.options.preventDuplicates = true;
        toastr.options.progressBar = true;
        toastr.options.onclick = function() {
            $('#feedbackPopup').modal('show');
            $('#feedbackPopup').find('tbody').children('tr:nth-child(2)').first().css('background-color', '#ffffc0 !important');

            setTimeout(function() {
                $('#feedbackPopup').find('tbody').children('tr:nth-child(2)').first().css('background-color', '#ffffff');
            }, 1000);
        };

        toastr["info"]("You have recieved a new review from " + $('#feedbackPopup').find('tbody').children('tr:nth-child(2)').first().children('td:nth-child(1)').text() + '. Click me to open it!');

        setCookie('lastReview', lastReview, 99999); //update it
    }

    function round(value, decimals) {
        return Number(Math.round(value + 'e' + decimals) + 'e-' + decimals);
    }

    var progressBar = $('#divActualProgress').parent();

    //removes arrows and text
    $(progressBar).parent().children('img').remove();

    $(progressBar).parent().children('span').remove();


    var totalLength = round($(progressBar).parent().width(), 0); //the total progress bar length

    var completedLength = round($('#divActualProgress').width(), 0); //the length that you've completed

    var expectedToBeAtLength = completedLength + round($('#divExtraProgress').width(), 0); //the length that you're meant to be at

    var wishedToBeAtLength = $('.wishCompleted').length + $('.unitBlock.completed').length; //needs to use getcookie as the class hasn't been implemented yet

    var totalUnitBlockLength = $('a.unitBlock').length;


    var completedPercentage = round(((completedLength / totalLength) * 100), 0); //the percentage that you've completed

    var expectedPercentage = round(((expectedToBeAtLength / totalLength) * 100), 0); //the percentage that you're meant to be at

    var wishedPercentage = round(((wishedToBeAtLength / totalUnitBlockLength) * 100), 0);


    console.log('Completed: ' + completedPercentage);

    console.log('Expected: ' + expectedPercentage);

    console.log('Wished: ' + wishedPercentage);


    var tempElem = $('#labelActualProgress').parent();

    $(tempElem).empty();

    $(tempElem).append('<div id="rootActualProgress" class="progress"></div>');

    $(tempElem).append('<div id="rootExpectedProgress" class="progress"></div>');

    $('#rootActualProgress').append('<div id="actual" class="progress-bar" role="progressbar" aria-valuenow="' + completedPercentage + '" aria-valuemin="0" aria-valuemax="100" style="width:' + completedPercentage + '%">Actual (' + round(completedPercentage, 0) + '%)</div>');

    $('#rootExpectedProgress').append('<div id="expected" class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="' + expectedPercentage + '" aria-valuemin="0" aria-valuemax="100" style="width:' + expectedPercentage + '%">Expected (' + round(expectedPercentage, 0) + '%)</div>');

    if (getCookie('completedUnits') !== '') //shows progress bar even when no units are ticked
    {
        $(tempElem).append('<div id="rootWishedProgress" class="progress"></div>');

        if (wishedPercentage === 100)
            $('#rootWishedProgress').append('<div id="wished" class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="' + wishedPercentage + '" aria-valuemin="0" aria-valuemax="100" style="width:' + wishedPercentage + '%">Ticked (and Completed) units (' + round(wishedPercentage, 0) + '%)</div>');
        else
            $('#rootWishedProgress').append('<div id="wished" class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="' + wishedPercentage + '" aria-valuemin="0" aria-valuemax="100" style="width:' + wishedPercentage + '%">Ticked (and Completed) units (' + round(wishedPercentage, 0) + '%)</div>');
    }

}

function messagingPage() {
    var newMessageURL = $('#lnkCompose a').attr('href');

    $('#lnkCompose a').attr('href', '#').attr('data-toggle', 'modal').attr('data-target', '#newMessageModal');

    //creates the modal
    $('body').append('<div id="newMessageModal" class="modal fade" tabindex="-1" role="dialog"></div>');
    $('#newMessageModal').append('<div class="modal-dialog"></div>');
    $('#newMessageModal div.modal-dialog').append('<div class="modal-content modal-lg" style="overflow-y: auto;"></div>');
    $('#newMessageModal div.modal-dialog div.modal-content').append('<div class="modal-header"><button type="button" class="close" data-dismiss="modal">&times;</button><h4 class="modal-title">New Message</h4></div>');
    $('#newMessageModal div.modal-dialog div.modal-content').append('<div class="modal-body"></div>');
    $('#newMessageModal div.modal-dialog div.modal-content').append('<div class="modal-footer"></div>');
    $('#newMessageModal div.modal-dialog div.modal-content div.modal-footer').append('<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>');

    $('#newMessageModal div.modal-dialog div.modal-content div.modal-body').load(newMessageURL + ' #frmNewMessage');

    $('body').append('<div id="hiddenScriptLoad" style="display: none !important"></div>');

    $('#hiddenScriptLoad').load(newMessageURL);
}
