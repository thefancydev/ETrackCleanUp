<h1>What is it?</h1>
This script goes hand in hand with a browser plugin called <a href="http://tampermonkey.net">TamperMonkey</a>. 
It's a script that hooks into my college's ETrack system and cleans up the UI and fixes quite a few bugs/quirks.

<h1>How does it work?</h1>
This script is injected dynamically by TamperMonkey, as a `<script>` tag at the bottom of the page. This way I can modify and manipulate the page using this script

<h1>How cool is it?</h1>
Very cool