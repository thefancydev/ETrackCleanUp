<h2>What you need:</h2>
<ul>
<li>Text Editor with a good JS/JQuery linter</li>
<li>Good JS and JQuery knowledge</li>
<li>Git</li>
<li>a gitlab account</li>
<li>A computer</li>
<li>A keyboard</li>
<li>A power source (for said computer)</li>
</ul>

<h2>How to contribute:</h2>
Warning: Before you embark on this, you need to remember that this website isn't the best, and that's why I wrote this, to make it better. I will only accept PR's that make ths website better for anyone using it.
<ol>
<li>Make sure you're logged in to Gitlab</li>
<li>Fork this repository</li>
<li>Clone your fork locally</li>
<li>Create a branch that describes your bugfix/improvement (e.g, <code>git branch AddedCuteCats</code>)</li>
<li>Commit your changes to that branch</li>
<li>Go to the original project page and create a 'Pull Request'</li>
<li>Select your feature branch</li>
<li>I will review the PR and merge it if it meets the above criteria</li>
</ol>